/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.settings.exception;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * Error code registry for the PRP  Settings Service. In this enum, every case used in this
 * Service is documented.
 * <p>
 * The purpose of these codes is to hand the reason for an {@link PRPException}
 * to the client or the calling service. This way, it is possible to create
 * understandable error messages.
 * <p>
 * <strong>Important to know:</strong> Not every error case explained here has
 * to be handed to the client as it is. Most of them, specially DAL errors,
 * indicate internal bugs. In this case, some general information should be
 * displayed to the user with the specific error code. This way, it is possible
 * to track bugs.
 *
 * @see PRPException
 * @author Eric Fischer
 * @since 0.1
 *
 */
public enum PRPErrorCode {

	/******************
	 * General errors *
	 *****************/

	/**
	 * This should be the default case in {@link LiaException}. If this error is
	 * handed to the client, something went completely wrong and should be treated
	 * as a bug.
	 *
	 * @since 0.1
	 */
	UNKNOWN("0"),

	/*******************
	 * Settings errors *
	 ******************/

	/**
	 * The DAL couldn't complete the request, because parameters are missing.
	 * <p>
	 * Definitely indicates a bug.
	 *
	 * @since 0.1
	 */
	BACKEND_SETTINGS_DAL_PARAMETERS_MISSING("B_S_D_PM"),
	/**
	 * The requested action could not be performed, because parameters were missing.
	 * <p>
	 * This typically is a bug within the client application and should be avoided.
	 *
	 * @since 0.1
	 */
	BACKEND_SETTINGS_BEAN_PARAMETERS_MISSING("B_S_B_PM"),
	/**
	 * The date received by the API isn't parsable.
	 * <p>
	 * This typically indicates a bug within the client application.
	 *
	 * @since 0.1
	 */
	BACKEND_SETTINGS_CONVERTER_INPUT_DATE_NOT_PARSABLE("B_S_C_IDNP");

	@Getter
	private final String code;

	PRPErrorCode(final String code) {
		this.code = code;
	}

}
