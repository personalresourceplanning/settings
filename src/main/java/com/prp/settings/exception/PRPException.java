/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.settings.exception;

import lombok.Getter;

/**
 * Standard exception type used in the system. This type contains the
 * {@link PRPErrorCode} defining the exact case of the exception.
 *
 * @see PRPErrorCode
 * @see Exception
 * @author Eric Fischer
 * @since 0.1
 *
 */
public class PRPException extends Exception {

	private static final long serialVersionUID = 5195587269929980795L;

	@Getter
	private PRPErrorCode errorCode = PRPErrorCode.UNKNOWN;

	/**
	 * Constructor for storing a message and a {@link PRPErrorCode} in the
	 * exception.
	 * <p>
	 * If no error code is provided, {@link PRPErrorCode#UNKNOWN} is set as default.
	 * This case indicates bugs and should not reach the client.
	 *
	 * @param message   - The {@link String} describing the cause of the exception.
	 * @param errorCode - The {@link PRPErrorCode} defining the cause of the
	 *                  exception.
	 * @since 0.1
	 * @see Exception
	 */
	public PRPException(final String message, final PRPErrorCode errorCode) {
		super(message);
		if (errorCode == null) {
			this.errorCode = PRPErrorCode.UNKNOWN;
		} else {
			this.errorCode = errorCode;
		}
	}

	/**
	 * Constructor for storing a message and a {@link PRPErrorCode} in the
	 * exception.
	 * <p>
	 * If no error code is provided, {@link PRPErrorCode#UNKNOWN} is set as default.
	 * This case indicates bugs and should not reach the client.
	 *
	 * @param message   - The {@link String} describing the cause of the exception.
	 * @param errorCode - The {@link PRPErrorCode} defining the cause of the
	 *                  exception.
	 * @param cause     - The {@link Throwable} that caused the exception.
	 * @see Exception
	 * @since 0.1
	 */
	public PRPException(final String message, final Throwable cause, final PRPErrorCode errorCode) {
		super(message, cause);
		if (errorCode == null) {
			this.errorCode = PRPErrorCode.UNKNOWN;
		} else {
			this.errorCode = errorCode;
		}
	}

}
