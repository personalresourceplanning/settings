/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.settings.util.converter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.prp.settings.exception.PRPErrorCode;
import com.prp.settings.exception.PRPException;
import com.prp.settings.api.pojo.ApiSleeptime;
import com.prp.settings.pojo.Sleeptime;

@Component
public final class SleeptimeConverter {

	private static Logger log = LoggerFactory.getLogger(SleeptimeConverter.class);

	public ApiSleeptime convert(final Sleeptime in) {
		Calendar start = new GregorianCalendar();
		start.setTime(in.getStart());
		Calendar end = new GregorianCalendar();
		end.setTime(in.getEnd());

		ApiSleeptime out = new ApiSleeptime();
		out.setStart(String.format("%2d:%2d", start.get(Calendar.HOUR_OF_DAY), start.get(Calendar.MINUTE)));
		out.setEnd(String.format("%2d:%2d", end.get(Calendar.HOUR_OF_DAY), end.get(Calendar.MINUTE)));

		return out;
	}

	public Sleeptime convert(final ApiSleeptime in) throws PRPException {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

		Sleeptime out = new Sleeptime();

		try {
			out.setStart(sdf.parse(in.getStart()));
			out.setEnd(sdf.parse(in.getEnd()));
		} catch (ParseException e) {
			String message = "Sleeptime input not parsable.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_SETTINGS_CONVERTER_INPUT_DATE_NOT_PARSABLE);
		}

		return out;
	}

}
