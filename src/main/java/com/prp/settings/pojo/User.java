/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.settings.pojo;

import com.prp.settings.security.enums.UserActivationState;
import com.prp.settings.security.enums.UserPermissions;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Model for transporting information about users.
 * <p>
 * Provided information are email, which is treated as the username in this application, password (not transported to clients and API
 * accessors), Accounts belonging to the user and it's permissions.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see UserActivationState
 * @see UserPermissions
 * @see Account
 */
@Data
@AllArgsConstructor
public class User {

	private Long id;

}
