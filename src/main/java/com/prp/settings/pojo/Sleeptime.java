/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.settings.pojo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.Data;

@Data
public class Sleeptime {

	/**
	 * Default constructor setting the default values for this setting. Remember to override the settings, if given.
	 *
	 */
	public Sleeptime() {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		try {
			this.start = sdf.parse("18:00");
			this.end = sdf.parse("08:00");
		} catch (ParseException e) {
			// can be ignored, because no variables are used here.
		}
	}

	private Date start;
	private Date end;

}
