/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.settings.bean;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.prp.settings.exception.PRPErrorCode;
import com.prp.settings.exception.PRPException;
import com.prp.settings.dal.SettingsDAL;
import com.prp.settings.dal.model.SleeptimeDB;
import com.prp.settings.pojo.Sleeptime;

@Component
public class UserSettingsBeanImpl implements UserSettingsBean {

	private static final Logger log = LoggerFactory.getLogger(UserSettingsBeanImpl.class);

	private SettingsDAL dal;

	@Autowired
	public UserSettingsBeanImpl(final SettingsDAL dal) {
		this.dal = dal;
	}

	@Override
	public void storeSleeptime(final Sleeptime sleeptime) throws PRPException {

		if (sleeptime == null || sleeptime.getStart() == null || sleeptime.getEnd() == null) {
			String message = "Not all parameters given to store the sleeptime settings.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_SETTINGS_BEAN_PARAMETERS_MISSING);
		}

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		Long userId = Long.valueOf(authentication.getName());

		Optional<SleeptimeDB> loaded = dal.loadSleeptime(userId);

		SleeptimeDB dbVal = null;
		if (loaded.isPresent()) {
			dbVal = loaded.get();
		} else {
			dbVal = new SleeptimeDB();
			dbVal.setUserId(userId);
		}
		dbVal.setStart(sleeptime.getStart());
		dbVal.setEnd(sleeptime.getEnd());
		dal.storeSleeptime(dbVal);
	}

	@Override
	public Sleeptime loadSleeptime() throws PRPException {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		Long userId = Long.valueOf(authentication.getName());

		Optional<SleeptimeDB> loaded = dal.loadSleeptime(userId);

		Sleeptime retVal = new Sleeptime();
		if (loaded.isPresent()) {
			SleeptimeDB dbVal = loaded.get();
			retVal.setStart(dbVal.getStart());
			retVal.setEnd(dbVal.getEnd());
		}

		return retVal;
	}

}
