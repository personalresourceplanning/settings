/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.settings.security;

import java.io.IOException;
import java.util.Collections;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoTokenServices;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.common.AuthenticationScheme;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.annotation.RequestScope;

import com.prp.settings.security.properties.SecurityProperties;

@Configuration
@EnableConfigurationProperties(SecurityProperties.class)
@EnableResourceServer
@EnableOAuth2Client
public class SecurityConfig extends ResourceServerConfigurerAdapter {

	private SecurityProperties securityProperties;

	@Autowired
	public SecurityConfig(final SecurityProperties securityProperties) {
		this.securityProperties = securityProperties;
	}

	@Override
	public void configure(final HttpSecurity http) throws Exception {
		//@formatter:off
		http.requestMatchers()
				.antMatchers("/user/**").and()
				.authorizeRequests()
				.antMatchers(HttpMethod.GET, "/swagger-ui/", "/v3/api-docs/**").permitAll()
				.antMatchers(HttpMethod.OPTIONS).permitAll()
				.anyRequest().authenticated();
		//@formatter:on
	}

	@Primary
	@Bean
	public RemoteTokenServices tokenService() {
		RemoteTokenServices tokenService = new RemoteTokenServices();
		tokenService.setCheckTokenEndpointUrl("http://localhost:8081/oauth/check_token");
		tokenService.setClientId("PRPBackend");
		tokenService.setClientSecret("secret");
		tokenService.setAccessTokenConverter(jwtAccessTokenConverter());
		return tokenService;
	}

	@Override
	public void configure(final ResourceServerSecurityConfigurer resources) {
		resources.tokenStore(tokenStore());
	}

	@Bean
	public TokenStore tokenStore() {
		return new JwtTokenStore(jwtAccessTokenConverter());
	}

	@Bean
	public JwtAccessTokenConverter jwtAccessTokenConverter() {
		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
		converter.setVerifierKey(getPublicKeyAsString());
		return converter;
	}

	private String getPublicKeyAsString() {
		try {
			return IOUtils.toString(securityProperties.getJwt().getPublicKey().getInputStream(), "UTF8");
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Bean
	@RequestScope
	public RestOperations restTemplate(final HttpServletRequest request) {
		RestTemplate restTemplate = new RestTemplate();
		String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
		restTemplate.setInterceptors(
				Collections.singletonList(new ResourceRequestInterceptor(authHeader, request.getCookies())));
		return restTemplate;
	}

	@Bean
	public PrincipalExtractor principalExtractor() {
		return new PRPPrincipalExctractor();
	}

	@Bean
	@RequestScope
	public OAuth2Authentication userInfo(final HttpServletRequest request) {
		UserInfoTokenServices services = new UserInfoTokenServices("http://localhost:8081/user/me", "PRPBackend");
		services.setPrincipalExtractor(principalExtractor());

		String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
		String tokenString = authHeader.replace("Bearer ", "").replace("bearer ", "");
		DefaultOAuth2AccessToken accessToken = new DefaultOAuth2AccessToken(authHeader);

		AuthorizationCodeResourceDetails resource = new AuthorizationCodeResourceDetails();
		resource.setAccessTokenUri("http://localhost:8081/oauth/token");
		resource.setClientId("PRPBackend");
		resource.setClientSecret("secret");
		resource.setUserAuthorizationUri("http://localhost:8081/oauth/authorize");
		resource.setAuthenticationScheme(AuthenticationScheme.header);
		resource.setClientAuthenticationScheme(AuthenticationScheme.header);

		OAuth2ClientContext clientContext = new DefaultOAuth2ClientContext();
		clientContext.setPreservedState("key", "abcdef");
		clientContext.setAccessToken(accessToken);

		OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(resource, clientContext);
		services.setRestTemplate(restTemplate);

		OAuth2Authentication authentication = services.loadAuthentication(authHeader);

		return authentication;
	}
}
