/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.settings.security;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.Cookie;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.StreamUtils;

public class ResourceRequestInterceptor implements ClientHttpRequestInterceptor {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	private String authHeader;
	private Cookie[] cookies;

	public ResourceRequestInterceptor(final String authHeader, final Cookie[] cookies) {
		this.authHeader = authHeader;
		this.cookies = cookies;
	}

	@Override
	public ClientHttpResponse intercept(final HttpRequest request, final byte[] body,
			final ClientHttpRequestExecution execution) throws IOException {
		if (authHeader != null) {
			request.getHeaders().add(HttpHeaders.AUTHORIZATION, authHeader);
		}
		if (cookies != null) {
			StringBuilder sb = new StringBuilder();
			List<Cookie> cookieList = Arrays.asList(cookies);
			Iterator<Cookie> iterator = cookieList.iterator();
			while (iterator.hasNext()) {
				Cookie cookie = iterator.next();
				sb.append(cookie.getName());
				sb.append(":");
				sb.append(cookie.getValue());
				if (iterator.hasNext()) {
					sb.append("; ");
				}
			}
			request.getHeaders().add(HttpHeaders.COOKIE, sb.toString());
		}
		logRequest(request, body);
		ClientHttpResponse response = execution.execute(request, body);
		logResponse(response);
		return response;
	}

	private void logRequest(final HttpRequest request, final byte[] body) throws IOException {
		if (log.isTraceEnabled()) {
			log.trace("===========================request begin================================================");
			log.trace("URI         : {}", request.getURI());
			log.trace("Method      : {}", request.getMethod());
			log.trace("Headers     : {}", request.getHeaders());
			log.trace("Request body: {}", new String(body, "UTF-8"));
			log.trace("==========================request end================================================");
		}
	}

	private void logResponse(final ClientHttpResponse response) throws IOException {
		if (log.isTraceEnabled()) {
			log.trace("============================response begin==========================================");
			log.trace("Status code  : {}", response.getStatusCode());
			log.trace("Status text  : {}", response.getStatusText());
			log.trace("Headers      : {}", response.getHeaders());
			log.trace("Response body: {}", StreamUtils.copyToString(response.getBody(), Charset.defaultCharset()));
			log.trace("=======================response end=================================================");
		}
	}
}
