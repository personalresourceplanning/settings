/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.settings.security.enums;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * Defining the permissions {@link User}s can have. The text values are used for
 * Spring Security's role definitions.
 *
 * @see User
 * @author Eric Fischer
 * @since 0.1
 *
 */
public enum UserPermissions {

	/**
	 * Admins can access special setup pages normal users are blocked from. Special
	 * Admin roles could be possible, too.
	 *
	 * @since 0.1
	 */
	ADMIN("ADMIN"),
	/**
	 * The standard user without any special permission.
	 *
	 * @since 0.1
	 */
	USER("USER");

	private static final String ROLE_PREFIX = "ROLE_";
	private static final Map<String, UserPermissions> typeMap = new HashMap<>();

	static {
		for (UserPermissions permission : values()) {
			typeMap.put(permission.getCode(), permission);
		}
	}

	@Getter
	private String code;

	private UserPermissions(final String code) {
		this.code = code;
	}

	/**
	 * Builds the Spring Security role name based on the underlying enum code.
	 *
	 * @return {@link String} - The Spring Security role name.
	 * @since 0.1
	 */
	public String getSpringSafeRole() {
		return ROLE_PREFIX + code;
	}

}
