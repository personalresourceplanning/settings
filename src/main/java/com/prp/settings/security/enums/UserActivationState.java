/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.settings.security.enums;

/**
 * Defining if an {@link User} is active or not.
 *
 * @see User
 * @author Eric Fischer
 * @since 0.1
 */
public enum UserActivationState {
	/**
	 * The user is active and not restricted for login.
	 *
	 * @since 0.1
	 */
	ACTIVE,
	/**
	 * The {@link User} was registered but isn't confirmed yet. Login should not be
	 * active without confirmation.
	 *
	 * @since 0.1
	 */
	WAITING_FOR_ACTIVATION,
	/**
	 * The {@link User} is locked and login is disabled. Locking a user could happen
	 * for security reasons or misuse of the application.
	 *
	 * @since 0.1
	 */
	LOCKED;
}
