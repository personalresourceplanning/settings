/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.settings.api;

import com.prp.settings.api.pojo.ApiExceptionResponse;
import com.prp.settings.exception.PRPErrorCode;
import com.prp.settings.exception.PRPException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionController extends ResponseEntityExceptionHandler {

	@ExceptionHandler(PRPException.class)
	protected ResponseEntity<Object> handlePRPException(PRPException ex, WebRequest request) {
		HttpStatus httpStatus;
		switch (ex.getErrorCode()) {
			case BACKEND_SETTINGS_BEAN_PARAMETERS_MISSING:
			case BACKEND_SETTINGS_CONVERTER_INPUT_DATE_NOT_PARSABLE:
				httpStatus = HttpStatus.BAD_REQUEST;
				break;
			default:
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		ApiExceptionResponse exceptionResponse = ApiExceptionResponse.builder()
				.error(ex.getClass().getName()).message(ex.getMessage()).status(httpStatus.value())
				.path(((ServletWebRequest) request).getRequest().getRequestURI())
				.internalErrorCode(ex.getErrorCode()).build();
		return handleExceptionInternal(ex, exceptionResponse, new HttpHeaders(), httpStatus,
				request);
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> defaultHandler(Exception ex, WebRequest request) {
		ApiExceptionResponse exceptionResponse = ApiExceptionResponse.builder()
				.error(ex.getClass().getName()).message(ex.getMessage())
				.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
				.path(((ServletWebRequest) request).getRequest().getRequestURI())
				.internalErrorCode(PRPErrorCode.UNKNOWN).build();
		return handleExceptionInternal(ex, exceptionResponse, new HttpHeaders(),
				HttpStatus.INTERNAL_SERVER_ERROR, request);
	}

}
