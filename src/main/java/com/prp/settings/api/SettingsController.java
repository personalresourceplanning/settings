/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.settings.api;

import com.prp.settings.pojo.Sleeptime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.prp.settings.exception.PRPException;
import com.prp.settings.api.pojo.ApiSleeptime;
import com.prp.settings.bean.UserSettingsBean;
import com.prp.settings.util.converter.SleeptimeConverter;

@RestController
@RequestMapping("/user")
public class SettingsController {

	private final UserSettingsBean userSettingsBean;
	private final SleeptimeConverter sleeptimeConverter;

	@Autowired
	public SettingsController(final UserSettingsBean userSettingsBean,
			final SleeptimeConverter sleeptimeConverter) {
		this.userSettingsBean = userSettingsBean;
		this.sleeptimeConverter = sleeptimeConverter;
	}

	@GetMapping("/sleeptime")
	public ResponseEntity<ApiSleeptime> loadSleeptime() throws PRPException {
		return new ResponseEntity<>(sleeptimeConverter.convert(userSettingsBean.loadSleeptime()),
				HttpStatus.OK);
	}

	@PostMapping("/sleeptime")
	public ResponseEntity<Sleeptime> storeSleeptime(@RequestBody final ApiSleeptime apiSleeptime)
			throws PRPException {
		userSettingsBean.storeSleeptime(sleeptimeConverter.convert(apiSleeptime));
		return new ResponseEntity<>(sleeptimeConverter.convert(apiSleeptime), HttpStatus.OK);
	}

}
