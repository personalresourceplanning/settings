/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.settings.api.pojo;

import com.prp.settings.exception.PRPErrorCode;
import lombok.*;

import java.util.Date;

/**
 * POJO used for responding to exceptions in the API
 *
 * Provides information to consumer about the Exception that occured.
 *
 * @author Robin Herder
 * @since 0.1
 * @see com.prp.settings.api.GlobalExceptionController
 */

@Data
@Builder
@AllArgsConstructor
public class ApiExceptionResponse {

	@Builder.Default
	private Date timestamp = new Date();

	private int status;

	private String error;

	private String message;

	private String path;

	private PRPErrorCode internalErrorCode;

}
