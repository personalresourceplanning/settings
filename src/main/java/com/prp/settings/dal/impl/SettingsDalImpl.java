/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.settings.dal.impl;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prp.settings.exception.PRPErrorCode;
import com.prp.settings.exception.PRPException;
import com.prp.settings.dal.SettingsDAL;
import com.prp.settings.dal.model.SleeptimeDB;
import com.prp.settings.dal.repository.SleeptimeRepository;

/**
 *
 * @author Eric Fischer
 * @since 0.1
 * @see SettingsDAL
 *
 */
@Component
public class SettingsDalImpl implements SettingsDAL {

	private static final Logger log = LoggerFactory.getLogger(SettingsDalImpl.class);

	private SleeptimeRepository sleeptimeRepository;

	@Autowired
	public SettingsDalImpl(final SleeptimeRepository sleeptimeRepository) {
		this.sleeptimeRepository = sleeptimeRepository;
	}

	@Override
	public Optional<SleeptimeDB> loadSleeptime(final Long userId) throws PRPException {
		if (userId == null || Long.valueOf(0).equals(userId)) {
			String message = "The user ID isn't given to load the sleeptime.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_SETTINGS_DAL_PARAMETERS_MISSING);
		}

		Optional<SleeptimeDB> foundSettings = sleeptimeRepository.findByUserId(userId);
		return foundSettings;
	}

	@Override
	public void storeSleeptime(final SleeptimeDB sleeptime) throws PRPException {
		if (sleeptime == null || sleeptime.getUserId() == null || Long.valueOf(0).equals(sleeptime.getUserId())
				|| sleeptime.getStart() == null || sleeptime.getEnd() == null) {
			String message = "The sleeptime or it's values are not given to store the setting.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_SETTINGS_DAL_PARAMETERS_MISSING);
		}
		sleeptimeRepository.save(sleeptime);
	}

}
