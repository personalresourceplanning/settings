/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.settings.dal.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Table(name = "prp_settings_sleeptime")
@Entity
@Data
public class SleeptimeDB {

	@Id
	@Column(name = "user", nullable = false)
	private Long userId;

	@Column(name = "start", nullable = false)
	@Temporal(TemporalType.TIME)
	private Date start;

	@Column(name = "end", nullable = false)
	@Temporal(TemporalType.TIME)
	private Date end;

}
