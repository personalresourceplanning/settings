/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.settings.dal;

import java.util.Optional;

import com.prp.settings.exception.PRPException;
import com.prp.settings.dal.model.SleeptimeDB;

/**
 * Data access layer for accessing PRP settings.
 *
 * @author Eric Fischer
 * @since 0.1
 */
public interface SettingsDAL {

	Optional<SleeptimeDB> loadSleeptime(Long userId) throws PRPException;

	void storeSleeptime(SleeptimeDB sleeptime) throws PRPException;

}
